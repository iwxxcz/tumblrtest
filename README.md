# README #

This README document whatever steps are necessary to get application up and running.

### What is this repository for? ###

* TumblrClient - small client for Tumblr.

### How do I get set up? ###

1. Download project
2. Run "pod install"
3. Run tests from xCode
4. Run project

### Stack ###

* Language: Swift 4 
* UI: XIBs + Autolayout
* Networking: Alamofire + PromiseKit
* Mapping: Encodable / Decodable (Swift 4)

### Features ###

PromiseKit simplify asynchronous programming and can be used in combination with most popular networking tools - Alamofire, OMGHTTPURLRQ, URLSession + DataTask.
Current networking layer setup with Alamofire, PromiseKit and Codable protocol onboard, gives you strong and flexible tool with easy to read and write code.

* PromiseKit - https://github.com/mxcl/PromiseKit
* Alamofire - https://github.com/Alamofire/Alamofire