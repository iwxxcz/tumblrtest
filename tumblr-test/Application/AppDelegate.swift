//
//  AppDelegate.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/29/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

