//
//  DataRequest + ResponseCodable.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/30/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

extension Alamofire.DataRequest {
    public func responseCodable<T: Decodable>() -> Promise<T> {
        return Promise { fulfill, reject in
            responseData(queue: nil) { response in
                switch response.result {
                case .success(let value):
                    do {
                        fulfill(try JSONDecoder().decode(T.self, from: value))
                    } catch let e {
                        reject(e)
                    }
                case .failure(let error):
                    reject(error)
                }
            }
        }
    }
}
