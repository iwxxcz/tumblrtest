//
//  Array+SafeSubscript.swift
//  FacebookPhotoViewer
//
//  Created by Serhii Bykov on 11/29/17.
//  Copyright © 2017 Serhii Bykov. All rights reserved.
//

import Foundation

extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
