//
//  UITableViewCell+DefaultReuseIdentifier.swift
//  
//
//  Created by Serhii Bykov on 11/28/17.
//

import UIKit

extension UITableViewCell {
    class var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}
