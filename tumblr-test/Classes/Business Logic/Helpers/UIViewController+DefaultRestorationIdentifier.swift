//
//  UIViewController+DefaultRestorationIdentifier
//
//  Created by Serhii Bykov on 11/28/17.
//  Copyright © 2017 Serhii Bykov. All rights reserved.
//

import UIKit

extension UIViewController {
    class var defaultRestorationIdentifier: String {
        return String(describing: self)
    }
}
