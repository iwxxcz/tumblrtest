//
//  Decodable + StringInit.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/31/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import Foundation

extension Decodable {
    static func object(from string: String) -> Self? {
        guard let data = string.data(using: .utf8) else { return nil }
        return try? JSONDecoder().decode(self, from: data)
    }
}
