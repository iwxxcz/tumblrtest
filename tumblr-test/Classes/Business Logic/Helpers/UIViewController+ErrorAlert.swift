//
//  UIViewController+Error.swift
//  FacebookPhotoViewer
//
//  Created by Serhii Bykov on 11/30/17.
//  Copyright © 2017 Serhii Bykov. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorAlert(with text: String) {
        let alertController = UIAlertController(title: "Error", message: text , preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
