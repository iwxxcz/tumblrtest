//
//  RequestError.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/30/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import Foundation

enum RequestError: Error {
    case networkError
    case cantResolveURL
    case dataIsEmpty
    case errorStatusCode
    case jsonNotParsed
    case custom(text: String)
}

extension RequestError: LocalizedError {
    var localizedDescription: String {
        switch self {
        case .networkError:
            return "Network error"
        case .cantResolveURL:
            return "Cannot resolve URL"
        case .dataIsEmpty:
            return "Data is empty"
        case .errorStatusCode:
            return "Error status code"
        case .jsonNotParsed:
            return "Couldn't get JSON"
        case .custom(let text):
            return text
        }
    }
}
