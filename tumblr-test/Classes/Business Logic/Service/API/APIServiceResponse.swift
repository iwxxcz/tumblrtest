//
//  APIServiceResponse.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/30/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import Foundation

class APIServiceMeta: Codable {
    var status: Int
    var message: String
    
    init(status: Int, message: String) {
        self.status = status
        self.message = message
    }
    
    enum CodingKeys: String, CodingKey {
        case status
        case message = "msg"
    }
}

class APIServiceResponse<T: Decodable>: Decodable {
    var meta: APIServiceMeta
    var value: T
    
    init(meta: APIServiceMeta, response: T) {
        self.meta = meta
        self.value = response
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        meta = try values.decode(APIServiceMeta.self, forKey: .meta)
        value = try values.decode(T.self, forKey: .response)
    }
    
    enum CodingKeys: String, CodingKey {
        case meta
        case response
    }
}
