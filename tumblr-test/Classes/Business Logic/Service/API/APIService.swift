//
//  APIService.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/29/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class APIService {
    static let shared = APIService()
    
    private init() { }
}

// MARK: - Constants
extension APIService {
    var APIKey: String {
        return "CcEqqSrYdQ5qTHFWssSMof4tPZ89sfx6AXYNQ4eoXHMgPJE03U"
    }
    
    var host: String {
        return "https://api.tumblr.com/"
    }
}

extension APIService {
    typealias PostsResult = Alamofire.Result<[PhotoPost]>
    typealias PostsResultCompletion = (PostsResult) -> Void

    func getPhotoPosts(by tag: String) -> Promise<APIServiceResponse<[PhotoPost]>> {
        let urlString = "\(host)v2/tagged?tag=\(tag)&api_key=\(APIKey)"
        return Alamofire.request(urlString, method: .get).validate().responseCodable()
    }
}
