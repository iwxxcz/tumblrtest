//
//  PhotoViewController.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/30/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController {
    @IBOutlet private weak var photoImageView: UIImageView!
    
    private var photo: Photo
    
    init(photo: Photo) {
        self.photo = photo
        super.init(nibName: PhotoViewController.defaultRestorationIdentifier, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.photo = Photo.emptyPhoto
        super.init(coder: aDecoder)
    }
}

// MARK: - Lifecycle
extension PhotoViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupData()
    }
}

// MARK: - Setups
private extension PhotoViewController {
    func setupData() {
        guard let url = URL(string: photo.originalSizeAsset.url) else { return }
        self.photoImageView.af_setImage(withURL: url)
    }
}
