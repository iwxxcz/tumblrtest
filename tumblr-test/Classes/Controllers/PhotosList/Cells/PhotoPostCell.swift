//
//  PostCell.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/29/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import UIKit
import AlamofireImage

class PhotoPostCell: UITableViewCell {
    @IBOutlet private weak var photoView: UIImageView!
    @IBOutlet private weak var authorNameLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoView.af_cancelImageRequest()
        photoView.image = nil
    }
    
    func configure(with post: PhotoPost) {
        authorNameLabel.text = post.authorName
        setPhoto(from: post)
    }
    
    private func setPhoto(from post: PhotoPost) {
        guard let photo = post.photos.first else { return }
        guard let url = URL(string: photo.originalSizeAsset.url) else { return }
        photoView.af_setImage(withURL: url)
    }
}
