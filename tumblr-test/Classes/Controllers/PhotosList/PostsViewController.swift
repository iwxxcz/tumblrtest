//
//  PostsViewController.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/29/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import UIKit
import SVProgressHUD
import PromiseKit

class PostsViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    private var posts: [Post] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }
}

// MARK: - Constants
private extension PostsViewController {
    var cellIdentifier: String {
        return PhotoPostCell.defaultReuseIdentifier
    }
    
    var cellHeight: CGFloat {
        return 312.0
    }
}

// MARK: - Setups
private extension PostsViewController {
    func setupTableView() {
        self.title = "Search"
        tableView.rowHeight = cellHeight
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.tableFooterView = UIView()
    }
}

// MARK: - Network
private extension PostsViewController {
    func startSearch(text: String) {
        firstly {
            APIService.shared.getPhotoPosts(by: text)
        }.then { apiResponse -> Void in
            self.posts = apiResponse.value.filter({ $0.kind == .photo })
            SVProgressHUD.dismiss()
            self.tableView.reloadData()
        }.catch { error in
            SVProgressHUD.dismiss()
            self.showErrorAlert(with: error.localizedDescription)
        }
    }
}

// MARK: - UITableView DataSource
extension PostsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PhotoPostCell else { return UITableViewCell() }
        guard let post = posts[safe: indexPath.row] as? PhotoPost else { return cell}
        cell.configure(with: post)
        return cell
    }
}

// MARK: UITableView Delegate
extension PostsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let photoPost = posts[safe: indexPath.row] as? PhotoPost else { return }
        guard let photo = photoPost.photos.first  else { return }
        showPhotoViewController(photo: photo)
    }
}

// MARK: - UITextField Delegate
extension PostsViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text, !text.isEmpty {
            self.view.endEditing(true)
            SVProgressHUD.show()
            startSearch(text: text)
        }
        else {
            self.showErrorAlert(with: "Tag cannot be empty")
        }
    }
}

// MARK: - Segues
private extension PostsViewController {
    func showPhotoViewController(photo: Photo) {
        let photoViewController = PhotoViewController(photo: photo)
        self.navigationController?.pushViewController(photoViewController, animated: true)
    }
}
