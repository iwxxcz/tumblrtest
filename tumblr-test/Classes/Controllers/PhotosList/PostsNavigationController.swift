//
//  PostsNavigationController.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/29/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import UIKit

class PostsNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRootController()
    }
    
    private func setupRootController() {
        let photosListViewController = PostsViewController()
        viewControllers = [photosListViewController]
    }
}
