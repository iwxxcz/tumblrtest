//
//  Photo.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/29/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import Foundation

class Post: Decodable {
    var authorName: String
    var timestamp: Double
    var kind: Kind
    
    private enum CodingKeys: String, CodingKey {
        case authorName = "blog_name"
        case timestamp = "timestamp"
        case kind = "type"
    }
    
    init(kind: Kind, authorName: String, timestamp: Double) {
        self.kind = kind
        self.authorName = authorName
        self.timestamp = timestamp
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        authorName = try values.decode(String.self, forKey: .authorName)
        timestamp = try values.decode(Double.self, forKey: .timestamp)
        let kindString = try values.decode(String.self, forKey: .kind)
        kind = Kind(rawValue: kindString) ?? .unknown
    }
}

extension Post {
    enum Kind: String {
        case unknown
        case text
        case photo
        case video
    }
}
