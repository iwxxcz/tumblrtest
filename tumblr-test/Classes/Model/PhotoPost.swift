//
//  PhotoPost.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/30/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import Foundation

class PhotoPost: Post {
    var photos: [Photo]
    
    private enum CodingKeys: String, CodingKey {
        case photos
    }
    
    init(kind: Post.Kind, authorName: String, timestamp: Double, photos: [Photo]) {
        self.photos = photos
        super.init(kind: kind, authorName: authorName, timestamp: timestamp)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            photos = try container.decode([Photo].self, forKey: .photos)
        }
        catch {
            self.photos = []
        }
        try super.init(from: decoder)
    }
}
