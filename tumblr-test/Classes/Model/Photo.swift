//
//  Photo.swift
//  tumblr-test
//
//  Created by Serhii Bykov on 1/30/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import Foundation

class Photo: Decodable {
    var caption: String
    var originalSizeAsset: Photo.Asset
    var altSizeAssets: [Photo.Asset]
    
    private enum CodingKeys: String, CodingKey {
        case caption
        case originalSizeAsset = "original_size"
        case altSizeAssets = "alt_sizes"
    }
    
    init(caption: String, originalSizeAsset: Photo.Asset, altSizeAssets: [Photo.Asset]) {
        self.caption = caption
        self.originalSizeAsset = originalSizeAsset
        self.altSizeAssets = altSizeAssets
    }
    
    class var emptyPhoto: Photo {
        return Photo(caption: "", originalSizeAsset: Photo.Asset.empty, altSizeAssets: [])
    }
}

// MARK: - Photo Asset
extension Photo {
    struct Asset: Decodable {
        var url: String
        var width: Int
        var height: Int
        
        init(url: String, width: Int, height: Int) {
            self.url = url
            self.width = width
            self.height = height
        }

        static var empty: Photo.Asset {
            return .init(url: "", width: 0, height: 0)
        }
    }
}
