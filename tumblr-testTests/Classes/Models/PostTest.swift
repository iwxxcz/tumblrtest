//
//  PostTest.swift
//  tumblr-testTests
//
//  Created by Serhii Bykov on 1/31/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import XCTest
@testable import tumblr_test

class PostTest: XCTestCase { }

extension PostTest {
    func testEmptyInput() {
        let stringData =
        """
            { }
        """
        XCTAssertNil(Post.object(from: stringData))
    }
    
    func testPartialInput() {
        let stringData =
        """
            {
                "blog_name": "blogName1",
                "timestamp": 1517310039
        """
        XCTAssertNil(Post.object(from: stringData))
    }
    
    func testIncorrectInput() {
        let stringData =
        """
            {
                "blog_name": "blogName1",
                "timestamp": "1517310039",
                "type": "text"
            }
        """
        XCTAssertNil(Post.object(from: stringData))
    }
    
    func testSuccess() {
        let stringData =
        """
            {
                "blog_name": "blogName1",
                "timestamp": 1517310039,
                "type": "text"
            }
        """
        let post = Post.object(from: stringData)!
        XCTAssertTrue(post.authorName == "blogName1")
        XCTAssertTrue(post.timestamp == 1517310039)
        XCTAssertTrue(post.kind == .text)
    }
}
