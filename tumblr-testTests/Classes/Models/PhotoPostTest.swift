//
//  PhotoPostTest.swift
//  tumblr-testTests
//
//  Created by Serhii Bykov on 1/31/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import XCTest
@testable import tumblr_test

class PhotoPostTest: XCTestCase { }

extension PhotoPostTest {
    func testEmptyInput() {
        let stringData =
        """
            { }
        """
        XCTAssertNil(PhotoPost.object(from: stringData))
    }
    
    func testPartialInput() {
        let stringData =
        """
            {
                "blog_name": "blogName1",
                "timestamp": 1517310039
        """
        XCTAssertNil(PhotoPost.object(from: stringData))
    }
    
    func testIncorrectInput() {
        let stringData =
        """
            {
                "blog_name": "blogName1",
                "timestamp": "1517310039",
                "type": "text"
            }
        """
        XCTAssertNil(PhotoPost.object(from: stringData))
    }
    
    func testSuccess() {
        let stringData =
        """
            {
                "blog_name": "blogName1",
                "timestamp": 1517310039,
                "type": "photo",
                "photos": [
                    {
                        "caption": "TestCaption1",
                        "original_size": {
                            "url": "1_500.jpg",
                            "width": 500,
                            "height": 500
                        },
                        "alt_sizes": [
                        {
                            "url": "1_250.jpg",
                            "width": 250,
                            "height": 250
                        }
                      ]
                    }
                ]
            }
        """
        let photoPost = PhotoPost.object(from: stringData)!
        XCTAssertTrue(photoPost.authorName == "blogName1")
        XCTAssertTrue(photoPost.timestamp == 1517310039)
        XCTAssertTrue(photoPost.kind == .photo)
        XCTAssertTrue(photoPost.photos.count == 1)
        XCTAssertTrue(photoPost.photos.first!.caption == "TestCaption1")
        XCTAssertTrue(photoPost.photos.first!.originalSizeAsset.url == "1_500.jpg")
        XCTAssertTrue(photoPost.photos.first!.originalSizeAsset.width == 500)
        XCTAssertTrue(photoPost.photos.first!.originalSizeAsset.height == 500)
    }
}
