//
//  PhotoTest.swift
//  tumblr-testTests
//
//  Created by Serhii Bykov on 1/31/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import XCTest
@testable import tumblr_test

class PhotoTest: XCTestCase { }

extension PhotoTest {
    func testEmptyInput() {
        let stringData =
        """
            { }
        """
        XCTAssertNil(Photo.object(from: stringData))
    }
    
    func testPartialInput() {
        let stringData =
        """
            {
                "caption": "TestCaption1",
                "original_size": {
                    "url": "1_500.jpg",
                    "width": 500,
                    "height": 500
                },
                "alt_sizes": [
                {
                    "url": "1_250.jpg",
                    "width"
        """
        XCTAssertNil(Photo.object(from: stringData))
    }
    
    func testIncorrectInput() {
        let stringData =
        """
            {
                "caption": 0,
                "original_size": {
                    "url": "1_500.jpg",
                    "width": 500,
                    "height": 500
                },
                "alt_sizes": [
                {
                    "url": "1_250.jpg",
                    "width": 250,
                    "height": 250
                }
              ]
            }
        """
        XCTAssertNil(Photo.object(from: stringData))
    }
    
    func testSuccess() {
        let stringData =
        """
            {
                "caption": "TestCaption1",
                "original_size": {
                    "url": "1_500.jpg",
                    "width": 500,
                    "height": 500
                },
                "alt_sizes": [
                {
                    "url": "1_250.jpg",
                    "width": 250,
                    "height": 250
                }
              ]
            }
        """
        if let photo = Photo.object(from: stringData) {
            XCTAssertTrue(photo.caption == "TestCaption1")
            XCTAssertTrue(photo.originalSizeAsset.width == 500)
            XCTAssertTrue(photo.originalSizeAsset.height == 500)
            XCTAssertTrue(photo.originalSizeAsset.url == "1_500.jpg")
            XCTAssertTrue(photo.altSizeAssets.count == 1)
            let altSizeAssetFirst = photo.altSizeAssets.first!
            XCTAssertTrue(altSizeAssetFirst.url == "1_250.jpg")
            XCTAssertTrue(altSizeAssetFirst.width == 250)
            XCTAssertTrue(altSizeAssetFirst.height == 250)
        }
        else {
            XCTAssert(false, "Photo not parsed")
        }
    }
}
