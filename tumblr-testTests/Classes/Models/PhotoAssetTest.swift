//
//  PhotoTest.swift
//  tumblr-testTests
//
//  Created by Serhii Bykov on 1/30/18.
//  Copyright © 2018 Serhii Bykov. All rights reserved.
//

import XCTest
@testable import tumblr_test

class PhotoAssetTest: XCTestCase { }

extension PhotoAssetTest {
    func testEmptyInput() {
        let stringData =
        """
            { }
        """
        XCTAssertNil(Photo.Asset.object(from: stringData))
    }
    
    func testPartialInput() {
        let stringData =
        """
            {
                "url": "http://google.com/",
                "width": "500",
        """
        XCTAssertNil(Photo.Asset.object(from: stringData))
    }
    
    func testIncorrectWidth() {
        let stringData =
        """
            {
                "url": "http://google.com/",
                "width": "500",
                "height": 500
            }
        """
        XCTAssertNil(Photo.Asset.object(from: stringData))
    }
    
    func testSuccess() {
        let stringData =
        """
            {
                "url": "http://google.com/",
                "width": 500,
                "height": 500
            }
        """
        let photoAsset = Photo.Asset.object(from: stringData)!
        XCTAssertTrue(photoAsset.url == "http://google.com/")
        XCTAssertTrue(photoAsset.width == 500)
        XCTAssertTrue(photoAsset.height == 500)
    }
}
